package io.ds.date.client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class DateClient {

    private static final int SERVER_PORT = 6789;

    /*
     * Program parameter 
     * args[0]: date format style 
     * args[1]: hostname of server
     */
    public static void main(String[] args) {
        String style = "SHORT"; // Default style
        String serverName = "localhost"; // Default hostname

        switch (args.length) {
        case 1:
            style = args[0].toUpperCase();
            break;
        case 2:
            style = args[0];
            serverName = args[1];
            break;
        default:
        }

        // Validate Style Input
        if (!style.equals("FULL") && !style.equals("MEDIUM") && !style.equals("SHORT")) {
            System.err.println("Style not valid!");
            System.exit(1);
        }
        // Validate hostname
        InetAddress aHost = null;
        try {
            aHost=InetAddress.getByName(serverName);
        } catch (UnknownHostException e) {
            System.err.println("Client: cannot resolve hostname!");
            e.printStackTrace();
            System.exit(1);
        }

        try (DatagramSocket aSocket = new DatagramSocket()) {
            //Send request
            byte[] m = style.getBytes();            
            DatagramPacket request = new DatagramPacket(m, m.length, aHost, SERVER_PORT);
            aSocket.send(request);

            //Receive reply
            byte[] buffer = new byte[1000];
            DatagramPacket reply = new DatagramPacket(buffer, buffer.length);
            aSocket.setSoTimeout(2000);
            aSocket.receive(reply);

            //Output result to STDIN
            System.out.println(new String(reply.getData()));
        } catch (SocketException e) {
            System.err.println("Client: can't open socket.");
        } catch (IOException e) {
            System.err.println("Client: received timeout for socket!");
            e.printStackTrace();
        }
    }
}
