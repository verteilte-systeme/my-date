# Exercise 1.1
This is a solution to exercise 1.1 (Date Service basted on UDP) of the hands-on training of lecture **distributed systems**.

Each folder contains a gradle project.

Start the server from a CLI (like bash, cmd, powershell) as follows:
* `$ cd server`
* `$ gradle run`

if you have installed gradle on your system, otherwise use the gradle wrapper contained in the folder (gradlew.bat, gradlew).

The server listens on port 6789.

When the server is up and running, you can start the client from a CLI as often you want. The client accepts several parameter:
- `args[0]: style ("SHORT", "MEDIUM" or "FULL")`
- `args[1]: hostname of server`

When you start the client without passing any parameter, the default values are "SHORT" and "localhost".

* `$ cd client`
* `$ gradle run --args="FULL localhost"`
