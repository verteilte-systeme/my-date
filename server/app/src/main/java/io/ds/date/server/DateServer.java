package io.ds.date.server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;

public class DateServer {

    private static final DateFormat FULL_DATE_FORMAT = DateFormat.getDateInstance(DateFormat.FULL);
    private static final DateFormat MEDIUM_DATE_FORMAT = DateFormat.getDateInstance(DateFormat.MEDIUM);
    private static final DateFormat SHORT_DATE_FORMAT = DateFormat.getDateInstance(DateFormat.SHORT);

    public static void main(String[] args) {

        byte[] buffer = new byte[1000];
        try (DatagramSocket serverSocket = new DatagramSocket(6789)) {
            System.out.println("Server listening ...");
            while (true) {
                try {
                    // Receive Request
                    DatagramPacket request = new DatagramPacket(buffer, buffer.length);
                    serverSocket.receive(request);// blocking!
                    String style = new String(request.getData()).trim();// trim eliminates the 0 bytes
                    System.out.println("Received request for style: " + style);
                    String dateString = getStyledDate(style);

                    // Send Reply
                    System.out.println("Send reply!");
                    byte[] dateBytes = dateString.getBytes();
                    DatagramPacket reply = new DatagramPacket(dateBytes,
                            dateBytes.length,
                            request.getAddress(),
                            request.getPort());
                    serverSocket.send(reply);
                } catch (IllegalStyleException e) {
                    System.err.println("Server: Illegal style, ignoring client request");
                }
                // Init buffer for reuse
                Arrays.fill(buffer, (byte) 0);
            }
        } catch (SocketException e) {
            System.err.println("Server: Can't open socket! Port in use?");
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("Server: Can't read from or write to socket!");
            e.printStackTrace();
        }
    }

    private static String getStyledDate(String style) throws IllegalStyleException {
        switch (style) {
            case "FULL":
                return FULL_DATE_FORMAT.format(new Date());
            case "MEDIUM":
                return MEDIUM_DATE_FORMAT.format(new Date());
            case "SHORT":
                return SHORT_DATE_FORMAT.format(new Date());
            default:
                throw new IllegalStyleException();
        }
    }
}
