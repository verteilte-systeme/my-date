package io.ds.date.server;

public class IllegalStyleException extends Exception {

    public IllegalStyleException() {
    }

    public IllegalStyleException(String message) {
        super(message);
    }

    public IllegalStyleException(Throwable cause) {
        super(cause);
    }

    public IllegalStyleException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalStyleException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
